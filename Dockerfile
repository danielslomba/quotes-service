# Use an official OpenJDK runtime as a parent image
FROM openjdk:17-ea-3-slim

# Set the working directory in the container
WORKDIR /app

# Copy the packaged JAR file into the container at /app
COPY target/quotes-0.0.1-SNAPSHOT.jar /app/quotes-0.0.1-SNAPSHOT.jar

# Expose the port your application will run on (default is 8080)
EXPOSE 8080

# Specify the command to run your application
CMD ["java", "-jar", "quotes-0.0.1-SNAPSHOT.jar"]
