package com.tui.challenge.quotes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigTest {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
