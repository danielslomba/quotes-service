package com.tui.challenge.quotes;

import com.tui.challenge.quotes.controller.QuoteController;
import com.tui.challenge.quotes.dto.QuoteDTO;
import com.tui.challenge.quotes.service.QuoteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class QuoteControllerTest {

    @InjectMocks
    private QuoteController quoteController;

    @Mock
    private QuoteService quoteService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(quoteController).build();
    }

    @Test
    void testGetQuoteById() throws Exception {
        // Create a sample quote
        QuoteDTO sampleQuote = new QuoteDTO("1", "Sample Quote", "Sample Author", "Quote Genre", 0);

        // Mock the behavior of the quoteService.findById method
        when(quoteService.findById("1")).thenReturn(Optional.of(sampleQuote));

        // Initialize the MockMvc instance
        mockMvc = MockMvcBuilders.standaloneSetup(quoteController).build();

        // Perform the GET request to /quotes/1 and expect a 200 OK response
        mockMvc.perform(MockMvcRequestBuilders.get("/quotes/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$._id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.quoteText").value("Sample Quote"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.quoteAuthor").value("Sample Author"));
    }

    @Test
    void testGetQuoteByAuthor() throws Exception {
        // Create sample quotes by the same author
        QuoteDTO quote1 = buildQuoteDTO("1", "Sample Quote 1", "SampleAuthor", "Life", 0);
        QuoteDTO quote2 = buildQuoteDTO("2", "Sample Quote 2", "SampleAuthor", "Age", 0);

        List<QuoteDTO> quotesByAuthor = Arrays.asList(quote1, quote2);

        when(quoteService.findByAuthor("SampleAuthor")).thenReturn(quotesByAuthor);

        mockMvc = MockMvcBuilders.standaloneSetup(quoteController).build();

        mockMvc.perform(MockMvcRequestBuilders.get("/quotes/author/SampleAuthor"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]._id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]._id").value("2"));
    }

    @Test
    void testGetAllQuotes() throws Exception {
        // Create sample quotes
        QuoteDTO quote1 = buildQuoteDTO("1", "Quote 1", "Author 1", "Life", 1);
        QuoteDTO quote2 = buildQuoteDTO("2", "Quote 2", "Author 2", "Motivation", 1);
        List<QuoteDTO> allQuotes = Arrays.asList(quote1, quote2);

        when(quoteService.findAll()).thenReturn(allQuotes);

        mockMvc.perform(MockMvcRequestBuilders.get("/quotes"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]._id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].quoteText").value("Quote 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].quoteAuthor").value("Author 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]._id").value("2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].quoteText").value("Quote 2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].quoteAuthor").value("Author 2"));
    }

    private QuoteDTO buildQuoteDTO(String id, String text, String author, String genre, int version) {
        return new QuoteDTO(id, text, author, genre, version);
    }
}
