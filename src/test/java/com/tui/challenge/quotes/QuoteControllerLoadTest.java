package com.tui.challenge.quotes;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.io.Resource;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class QuoteControllerLoadTest {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restTemplate;

    @Value("classpath:quote_ids.txt")
    private Resource idsResource;

    @Value("classpath:quote_authors.txt")
    private Resource authorsResource;
    private String baseUrl;

    @BeforeEach
    public void setUp() {
        baseUrl = "http://localhost:" + port + "/quotes";
    }

    @Test
    void testGetQuoteByIdConcurrently() throws InterruptedException, ExecutionException, TimeoutException, IOException {
        List<String> ids = readFromFile(idsResource);
        List<CompletableFuture<Long>> futures = ids.stream()
                .map(id -> startRequest(baseUrl + "/" + id))
                .collect(Collectors.toList());

        CompletableFuture<Void> allOf = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        allOf.get(10, TimeUnit.SECONDS); // Adjust the timeout as needed
        collectAndPrintResponseTimes(futures);
    }

    @Test
    void testGetQuoteByAuthorConcurrently() throws InterruptedException, ExecutionException, TimeoutException, IOException {
        List<String> authors = readFromFile(authorsResource);
        List<CompletableFuture<Long>> futures = authors.stream()
                .map(author -> startRequest(baseUrl + "/author/" + author))
                .collect(Collectors.toList());

        CompletableFuture<Void> allOf = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        allOf.get(10, TimeUnit.SECONDS);
        collectAndPrintResponseTimes(futures);
    }

    @Test
    void testGetAllQuotesConcurrently() throws InterruptedException, ExecutionException, TimeoutException {
        List<CompletableFuture<Long>> futures = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            CompletableFuture<Long> future = startRequest(baseUrl);
            futures.add(future);
        }

        CompletableFuture<Void> allOf = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        allOf.get(240, TimeUnit.SECONDS);
        collectAndPrintResponseTimes(futures);
    }

    private CompletableFuture<Long> startRequest(String url) {
        // Create a CompletableFuture to measure response time
        return CompletableFuture.supplyAsync(() -> {
            long startTime = System.currentTimeMillis();
            // Send a GET request to the URL
            restTemplate.getForObject(url, String.class);
            long endTime = System.currentTimeMillis();
            return endTime - startTime;
        });
    }

    private static void collectAndPrintResponseTimes(List<CompletableFuture<Long>> futures) {
        List<Long> responseTimes = futures.stream()
                .map(f -> {
                    try {
                        return f.get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                        return -1L;
                    }
                })
                .collect(Collectors.toList());

        // Print response times
        for (int i = 0; i < responseTimes.size(); i++) {
            log.info("Request " + i + " - Response Time: " + responseTimes.get(i) + " ms");
        }
        log.info("Average response time: {} ms", responseTimes.stream().mapToLong(Long::longValue).average().getAsDouble());
    }

    private List<String> readFromFile(org.springframework.core.io.Resource resource) throws IOException {
        List<String> ids = new ArrayList<>();
        try (InputStream inputStream = resource.getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                ids.add(line);
            }
        }
        return ids;
    }
}