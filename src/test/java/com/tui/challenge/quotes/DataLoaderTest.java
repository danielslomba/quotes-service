package com.tui.challenge.quotes;

import com.tui.challenge.quotes.repository.QuoteRepository;
import com.tui.challenge.quotes.service.MongoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DataLoaderTest {

    @Mock
    private MongoService mongoService;

    @Mock
    private QuoteRepository quoteRepository;

    @Mock
    DataLoaderProperties properties;

    private DataLoader dataLoader;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        dataLoader = new DataLoader(WebClient.builder().baseUrl(""), mongoService, properties, quoteRepository);
    }

    @Test
    void run_shouldLoadDataIntoMongoDB_whenCollectionIsEmpty() {
        // Arrange
        when(mongoService.hasDataInCollection("quotes")).thenReturn(0L);

        // Act
        dataLoader.run();

        // Assert
        verify(quoteRepository).saveAll(any());
    }

    @Test
    void run_shouldSkipLoading_whenCollectionIsNotEmpty() {
        // Arrange
        when(mongoService.hasDataInCollection("quotes")).thenReturn(2L);

        // Act
        dataLoader.run();

        // Assert
        verify(quoteRepository, never()).saveAll(any());
    }
}
