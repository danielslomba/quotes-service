package com.tui.challenge.quotes.dto;

public record QuoteDTO(String _id, String quoteText, String quoteAuthor, String quoteGenre, int __v) {
}
