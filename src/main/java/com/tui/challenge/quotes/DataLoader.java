package com.tui.challenge.quotes;

import com.tui.challenge.quotes.model.Quote;
import com.tui.challenge.quotes.repository.QuoteRepository;
import com.tui.challenge.quotes.service.MongoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DataLoader implements CommandLineRunner {
    private static final String API_BASE_URL = "https://quote-garden.onrender.com/api/v3/quotes";
    private final WebClient webClient;
    private final MongoService mongoService;
    @Autowired
    private final DataLoaderProperties properties;
    @Autowired
    private final QuoteRepository quoteRepository;

    @Autowired
    DataLoader(WebClient.Builder webClientBuilder, MongoService mongoService, DataLoaderProperties properties, QuoteRepository quoteRepository) {
        this.webClient = webClientBuilder.baseUrl(API_BASE_URL).build();
        this.mongoService = mongoService;
        this.properties = properties;
        this.quoteRepository = quoteRepository;
    }

    @Override
    public void run(String... args) {
        long countQuotesInCollection = mongoService.hasDataInCollection("quotes");
        if (countQuotesInCollection > 1) {
            log.info("database already loaded with {} items. Loading will be skipped.", countQuotesInCollection);
            return;
        }
        Flux<Quote> quotesFlux = getAllQuotes();

        // Collect the Flux into a List<Quote> and then save to MongoDB
        Mono<List<Quote>> quotesMono = quotesFlux.collectList();
        List<Quote> quotes = quotesMono.block();

        quoteRepository.saveAll(quotes);
        log.info("Data loaded into MongoDB.");
    }


    public Flux<Quote> getAllQuotes() {
        return fetchQuotesPage(properties.getStartPage(), properties.getMaxItemsPerPage())
                .flatMapMany(firstPageResponse -> {
                    Flux<QuotesApiResponse<Quote>> subsequentPages = fetchSubsequentPages(properties.getTotalPagesToFetch(), properties.getMaxItemsPerPage());
                    return Flux.concat(
                            Flux.fromIterable(firstPageResponse.getData()),
                            subsequentPages.flatMap(response -> Flux.fromIterable(response.getData()).subscribeOn(Schedulers.parallel()))
                    );
                });
    }

    private Mono<QuotesApiResponse<Quote>> fetchQuotesPage(Integer page, int limit) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(API_BASE_URL);
        uriBuilder.queryParam("page", page);
        uriBuilder.queryParam("limit", limit);
        return webClient.get()
                .uri(uriBuilder.buildAndExpand().toUri())
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<QuotesApiResponse<Quote>>() {
                }).subscribeOn(Schedulers.parallel());
    }

    private Flux<QuotesApiResponse<Quote>> fetchSubsequentPages(int totalPages, int maxItemsPerPage) {
        List<Mono<QuotesApiResponse<Quote>>> pageMonos = new ArrayList<>();
        for (int page = 2; page <= totalPages; page++) {
            pageMonos.add(fetchQuotesPage(page, maxItemsPerPage));
        }
        return Flux.merge(pageMonos);
    }


}
