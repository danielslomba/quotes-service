package com.tui.challenge.quotes.repository;

import com.tui.challenge.quotes.model.Quote;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuoteRepository extends MongoRepository<Quote, String> {
    List<Quote> findByQuoteAuthor(String author);
}
