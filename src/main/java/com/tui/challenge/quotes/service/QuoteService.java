package com.tui.challenge.quotes.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tui.challenge.quotes.dto.QuoteDTO;
import com.tui.challenge.quotes.model.Quote;
import com.tui.challenge.quotes.repository.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuoteService {
    private final QuoteRepository quoteRepository;
    private final ObjectMapper objectMapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    @Autowired
    public QuoteService(QuoteRepository quoteRepository) {
        this.quoteRepository = quoteRepository;
    }

    public Optional<QuoteDTO> findById(String id) {
        Optional<Quote> quote = quoteRepository.findById(id);
        return quote.map(value -> objectMapper.convertValue(value, QuoteDTO.class));
    }

    public List<QuoteDTO> findByAuthor(String author) {
        List<Quote> byQuoteAuthor = quoteRepository.findByQuoteAuthor(author);
        return objectMapper.convertValue(byQuoteAuthor, new TypeReference<>() {
        });
    }

    public List<QuoteDTO> findAll() {
        List<Quote> quotes = quoteRepository.findAll();
        return objectMapper.convertValue(quotes, new TypeReference<>() {
        });
    }
}