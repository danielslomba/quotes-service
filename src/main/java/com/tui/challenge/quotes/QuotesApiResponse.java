package com.tui.challenge.quotes;

import lombok.Data;

import java.util.List;
@Data
public class QuotesApiResponse<T> {
        private int statusCode;
        private String message;
        private Pagination pagination;
        private int totalQuotes;
        private List<T> data;

        @Data
        public static class Pagination {
            private int currentPage;
            private int nextPage;
            private int totalPages;
        }
    }

