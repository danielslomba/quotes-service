package com.tui.challenge.quotes.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "quotes")
public class Quote {
    @Id
    private String _id;
    private String quoteText;
    //Maybe it is worth to annotate with Indexed as we do a lot of calls from the author
    private String quoteAuthor;
    private String quoteGenre;
    private int __v;
}
