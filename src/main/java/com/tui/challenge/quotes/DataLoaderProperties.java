package com.tui.challenge.quotes;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "loader")
@EnableAutoConfiguration
public class DataLoaderProperties {

    private int maxNumberOfQuotes;
    @Min(1)
    private int startPage;
    @Max(5000)
    private int maxItemsPerPage;

    public int getTotalPagesToFetch() {
        return (maxNumberOfQuotes / maxItemsPerPage) - startPage;
    }
    
    public int getMaxNumberOfQuotes() {
        return maxNumberOfQuotes;
    }

    public void setMaxNumberOfQuotes(int maxNumberOfQuotes) {
        this.maxNumberOfQuotes = maxNumberOfQuotes;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getMaxItemsPerPage() {
        return maxItemsPerPage;
    }

    public void setMaxItemsPerPage(int maxItemsPerPage) {
        this.maxItemsPerPage = maxItemsPerPage;
    }
}
