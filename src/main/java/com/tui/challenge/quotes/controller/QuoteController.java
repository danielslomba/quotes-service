package com.tui.challenge.quotes.controller;

import com.tui.challenge.quotes.dto.QuoteDTO;
import com.tui.challenge.quotes.service.QuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/quotes")
public class QuoteController {

    private final QuoteService quoteService;

    @Autowired
    public QuoteController(QuoteService quoteService) {
        this.quoteService = quoteService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuoteDTO> getQuoteById(@PathVariable String id) {
        Optional<QuoteDTO> quoteDTO = quoteService.findById(id);
        return quoteDTO.map(dto -> new ResponseEntity<>(dto, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/author/{author}")
    public ResponseEntity<List<QuoteDTO>> getQuoteByAuthor(@PathVariable String author) {
        List<QuoteDTO> byAuthor = quoteService.findByAuthor(author);
        if(byAuthor.isEmpty()) {
           return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(byAuthor, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<QuoteDTO>> getAllQuotes() {
        List<QuoteDTO> all = quoteService.findAll();
        return new ResponseEntity<>(all, HttpStatus.OK);
    }
}
