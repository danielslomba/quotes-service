# Quotes API

This application provides an API to query quotes from a mongoDb database.
The total number of items is 50000.

When the application starts, it will verify if the db already contains data.
If it does, it will skip the step to load the database from the quotes api
provided: https://pprathameshmore.github.io/QuoteGarden/#get-quotes
Notice that, as this is for testing purposes, I decided to have a DB that runs on docker on your local machine.

## To run the application

- docker-compose up -d
    - to start local mongoDB
- mvn clean install
- run QuotesApplication
    - On the first run, the application will take more time to start as it will load the DB with 50000 items

## To run the tests

- after the docker-compose up -d (mongoDB pod is running)
    - right click on package com.tui.challenge.quotes and 'Run tests in "quotes"'
    - or mvn clean install
    - or clean install from the Maven plugin from intellij
    - ![img.png](img.png)
- there is a class under helper package in the test folder that will clean the mongoDB collection
    - just open DbHelper class and run cleanDb test

## To query the API

- if the application is running, using postman you can query the api on following paths:
  - localhost:8080/quotes - return all quotes
  - localhost:8080/quotes/author/${author} - return quotes of an author
  - localhost:8080/quotes/${id} - return quote by id

### Considerations

When I started doing this challenge, I wrongly assumed that I should query the API provided and not the mongoDB.
This made me try for the first time the framework
WebFlux: https://docs.spring.io/spring-framework/reference/web/webflux.html.
So I built this application that queries directly the quotes api, but the non-functional requirements were not met. Even
with reactive programing.
I made the change from using the RESTTemplate and use this new framework to leverage it's reactive capabilities. It is
worth noticing that the improvement was big on the calls to the api.

After asking for confirmation if I should query the api or just load the db, I got the response from Thiago Fogar, that
I should load the
db.
As I had already a lot of code using WebFlux I then decided to implement the DataLoader class while using it.
The rest of the application was more or less the same as the first.

It took me 4 hours Saturday, 4/5 hours Sunday, and 1/2 hours on Monday and Tuesday.
Maybe it took a bit more time than expected, but I learnt about WebFlux which I never used before and also needed to
redo part of the first application as it was using the api as endpoint and not the db.

It is worth to remark that are still things that have room for improvement:

- I could have a single db only for testing.
- some properties are still harcoded
- code could be refactored a bit more
- logging on the application could be improved
- custom exception handling
- a proper api documentation (using swagger?) would be appreciated
- etc...
